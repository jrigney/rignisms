---
title: Digital Ocean OpenVPN
tags:
---

So I needed to setup an OpenVPN server.  DigitalOcean happen to have a nice set of tutorials for Ubuntu.

[Ubuntu 14.04 tutorial](https://www.digitalocean.com/community/tutorials/how-to-set-up-an-openvpn-server-on-ubuntu-14-04)

[Ubuntu 16.04 tutorial](https://www.digitalocean.com/community/tutorials/how-to-set-up-an-openvpn-server-on-ubuntu-16-04)

[Docker OpenVPN on Ubuntu 14.04](https://www.digitalocean.com/community/tutorials/how-to-run-openvpn-in-a-docker-container-on-ubuntu-14-04)


