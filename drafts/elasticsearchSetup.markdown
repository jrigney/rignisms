---
title: Setting up a Dockerized Elasticsearch
tags: Elasticsearch Docker
---

## Install Elasticsearch & kopf plugin
I used the [official Elasticsearch](https://hub.docker.com/_/elasticsearch/) in Docker Hub as the base my own Dockerfile that installs Kopf.

## Configure Elasticsearch

There is a theme of not relying on the defaults.  I have been bitten many times thinking the default was one thing and it was another.

cluster.name

: You want to set this to be sure the name of your cluster because this is what is used to cluster your elasticsearch automatically.  If you get this wrong obviously your cluster will not be made up of the machines you want.

node.name

: It is best to give this the host name so it shows up in Kopf as something recognizable.

network.host

: You want to set this to be sure what the host is

http.port

: You want to set this to be sure what the port is

http.cors.enabled: true

: Not much to say about this you either need it or don't

http.cors.allow-origin: "*"

: Not much to say about this you either need it or don't

transport.publish_host

: The host address to publish for nodes in the cluster to connect to.  If you are running on something like Openstack that has a backplane network that is different from the external facing network you will need to set this because the nodes will not be able to communicate.  This becomes evident when using the Java client because the TransportClient communicates to the cluster in a manner that is similar to how all the cluster nodes communicate on port 9300.  Unlike other clients that use the rest interface on port 9200.  This one is interesting because it does not appear in the basic configuration of elastic.  It is part of the [transport module](https://www.elastic.co/guide/en/elasticsearch/reference/current/modules-transport.html).

transport.tcp.port

: This is used in conjunction with transport.publish_host.  Usually 9300

index.max_result_window

: control the amount af memory used for a search request

discovery.zen.ping.multicast.enabled: false

: I set this to false because it enabled on the network I am on.

discovery.zen.ping.unicast.hosts

: List of host that are part of the network.

##Run ElasticSearch
docker run -d --restart unless-stopped --name es1 -p 9200:9200 -p 9300:9300 -v /data:/usr/share/elasticsearch/data -v /opt/docker/etc/elasticsearch/config elasticsearch:2.3

This will run elastic in the background and restart it unless you stop it. It will export the port 9200 and 9300 so you can access it on the host machine. The data directory and the config directory will be also be exported to the host machine as well to /data and /opt/docker/etc/elasticsearch respectively.
