---
Title: Path finding using breadth first search
tags: algorithms
---

Using breadth first search build a spanning tree.
Using the spanning tree find the path

## Spanning Tree

1. Get all neighbors of the start Vertex.  To each neighbor add a distance and the parent Vertex.  The distance is the distance from the start Vertex
1. Remove any neighbors that have been visited.
1. if newNeighbors is empty then you gone through all the nodes.
1. else Add these new WeightedVertex to both the visited and the accumulator and call the function again using newNeighbors as the start.
1. The accumulator is your spanning tree.

~~~
type Graph = Set[AdjacencyList]
type Vertex = Int
case class AdjacencyList(vertex: Vertex, adjacentVertices: Set[Vertex])
case class WeightedVertex(vertex: Vertex, distance: Int, parentVertex: Int)

def spanningTree(start: Vertex, g: Graph): List[WeightedVertex] = {
  @scala.annotation.tailrec
  def spanningTreeLoop(start: List[WeightedVertex], visited: List[WeightedVertex], distance: Int, acc:List[WeightedVertex], graph: Graph): List[WeightedVertex] = {

    val potentialNeighbors:List[WeightedVertex] = start.flatMap{parentVertex =>
      graph.find(_.vertex == parentVertex.vertex).map{adjacencyList =>
        adjacencyList.adjacentVertices.map{vertex =>
          WeightedVertex(vertex, distance + 1, parentVertex.vertex)}
      }
    }.flatten

    val newNeighbors:List[WeightedVertex] = potentialNeighbors.filterNot(visited.contains).distinct
    if (newNeighbors.isEmpty) start ++ acc
    else spanningTreeLoop(newNeighbors, newNeighbors ++ visited, distance + 1, newNeighbors ++ acc, graph)
  }

  val startWeighted = WeightedVertex(start, 0, start)
  spanningTreeLoop(List(startWeighted), List(startWeighted), 0, List(startWeighted), g )

}
~~~
