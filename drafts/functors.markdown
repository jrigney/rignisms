---
title: Functors
tags: scala essentialScala functional
---

A Functor is a type that has a map capability (fmap in Haskell).  Option, List, Tree, and others in the Scala Collections library have a map capability.

~~~{.scala .numberLines}
val x = Some(1)
x.map(_+1)
~~~

Map is defined as def map[A,B](fa: F[A])(ab: A => B): F[B].  Where F is the functor.  At a high level map dips into the functor(fa) and executes a function(ab) on the value in the functor and returns the functor.  Sounds simple but the devil is in the details.  Function ab is A => B so the type in the functor can change.  That is why the return type is F[B].  So you are taking an F[A] and getting back an F[B].  The F is the Functor and remains the same.  Meaning you can not return another Functor type.  So let's get back to Functor.

~~~{.scala .numberLines}
trait Functor[F[_]] {
  def map[A,B](fa: F[A])(ab: A => B): F[B]
}
~~~

intuition functor A is a description of a computation that will return an A where A is a type constructor.
A functor F[A] is a description of a computation that returns an A.
A Functor[A] is a description af a computation that will return you a Functor[A]
A Functor[List] return Functor[List]
Remember the trait definition Functor[F[_]] F[_] is saying you need a type constructor * -> *.  So the map function takes an F[A] and returns an F[B] looks different but the F remains the same.
So basically a functor preserves the structure and lets you change the insides.


