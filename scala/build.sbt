lazy val commonSettings = Seq(
  name := "ScalaBlogs",
  organization := "jrigney",
  version := "0.1.0",
  scalaVersion := "2.11.8",
  shellPrompt := { s => Project.extract(s).currentProject.id + " > " },
  testOptions in Test += Tests.Argument("-oI"),
  scalacOptions in Compile ++= Seq(
                          "-deprecation",
                          "-encoding", "UTF-8",       // yes, this is 2 args
                          "-feature",
                          "-language:existentials",
                          "-language:higherKinds",
                          "-language:implicitConversions",
                          "-language:postfixOps",
                          "-language:reflectiveCalls",
                          "-unchecked",
                          "-Xfuture",
                          "-Xlint",
                          "-Yno-adapted-args",
                          "-Ywarn-value-discard")
)


val test = Seq(
"org.scalatest" % "scalatest_2.11" % "2.2.1" % "test",
"org.scalacheck" %% "scalacheck" % "1.12.2" % "test",
"junit" % "junit" % "4.12" % "test",
"org.hamcrest" % "hamcrest-all" % "1.3" % "test"
)

val logging = Seq(
"org.slf4j" % "slf4j-api" % "1.7.7",
"org.clapper" %% "grizzled-slf4j" % "1.0.2",
"ch.qos.logback" % "logback-classic" % "1.1.2"
)

val scalaz = Seq(
  "org.scalaz" %% "scalaz-core" % "7.2.6",
  "org.scalaz" %% "scalaz-concurrent" % "7.2.6"
)

val cats = Seq(
  "org.typelevel" %% "cats" % "0.7.2"
)

val http4sVersion = "0.15.3a"
val http4s = Seq (
  "org.http4s" %% "http4s-dsl" % http4sVersion,
  "org.http4s" %% "http4s-blaze-server" % http4sVersion,
  "org.http4s" %% "http4s-blaze-client" % http4sVersion
)


lazy val root = (project in file(".")).
  configs( IntegrationTest ).
  settings( Defaults.itSettings : _*).
  settings(commonSettings: _*).
  enablePlugins(TutPlugin).
  settings(
    libraryDependencies ++= test ++ logging ++ scalaz ++ cats ++ http4s
  )
