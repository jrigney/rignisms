
import scala.concurrent._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._


object Fac extends App {

  def facBefore() = {
    def slowly[A](body: => A) =
      try body finally Thread.sleep(100)

    def factorial(n: Int): Int = {
      val ans = slowly(if (n == 0) 1 else n * factorial(n - 1))
      println(s"fact $n $ans")
      ans
    }

    Await.result(Future.sequence(Vector(
      Future(factorial(5)),
      Future(factorial(5))
    )), Duration.Inf)
  }


  def facAfter() = {
    import cats.data.Writer
    import cats.syntax.applicative._
    import cats.syntax.writer._
    import cats.instances.vector._

    type Logged[A] = Writer[Vector[String], A]

    def slowly[A](body: => A): A = try body finally Thread.sleep(100)

    def factorial(n: Int): Logged[Int] = {
      if (n == 0) 1.pure[Logged]
      else {
        for {
          f <- slowly(factorial(n - 1))
          _ <- Vector(s"fact $n ${f*n}").tell
        } yield n * f
      }
    }

    val Vector((logA, ansA), ((logB, ansB))) = Await.result(Future.sequence(Vector(
      Future(factorial(5).run),
      Future(factorial(5).run)
    )), Duration.Inf)

    println(s"A: ${logA} $ansA")
    println(s"B: ${logB} $ansB")
  }

  facBefore()
  facAfter()
}

