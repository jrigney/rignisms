
def slowly[A](body: => A) = try body finally Thread.sleep(100)

def factorial(n: Int): Int = {
  val ans = slowly(if (n == 0) 1 else n * factorial(n - 1))
  println(s"fact $n $ans")
  ans
}
//factorial(5)

import scala.concurrent._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._

val a = Await.result(Future.sequence(Vector(
  Future(factorial(5)),
  Future(factorial(5))
)), Duration.Inf)
