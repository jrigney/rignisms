package h4s


object ClientExample extends App {

  import org.http4s.client.Client
  import org.http4s.client.blaze._
  import org.http4s.Uri
  import scalaz.concurrent.Task

  val httpClient: Client = PooledHttp1Client()

  def keyValue(value: String): Task[String] = {
    val target = Uri.uri("http://echo.jsontest.com/key") / value
    httpClient.expect[String](target)
  }

  val keys = Vector("apple", "butter", "charles", "duff")
  val values: Task[List[String]] = Task.gatherUnordered(keys.map(keyValue))

  val result = values.onFinish(_ => httpClient.shutdown).unsafePerformSync
  println(result)

}

