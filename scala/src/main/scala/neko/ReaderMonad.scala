package neko

import cats.Id
import cats.data.Kleisli

object ReaderMonad extends App {


  def createExtract() = {
    import cats.data.Reader
    val doubleReader: Reader[Int, Int] = Reader((x: Int) => x * 2)

    val double: Int => Int = doubleReader.run
  }

  def composing() = {
    import cats.data.Reader
    val doubleReader: Reader[Int, Int] = Reader((x: Int) => x * 2)
    val bang: Reader[Int, String] = doubleReader.map(x => x + "!")
    val theFn: Int => String = bang.run
    val bangString: String = theFn(10)


    //how is the different?  Why use the Reader?
    val h: Int => Int = (x: Int) => x * 2
    val i: Int => String = (x: Int) => x + "!"
    val j: Int => String = i compose h
    val k: String = j(10)
  }

  def composingFlat() = {
    import cats.data.Reader

    type SpecialNumber = Double

    val doubleReader: Reader[SpecialNumber, Int] = Reader((x: SpecialNumber) => x.toInt * 2)
    val bang: Reader[SpecialNumber, String] = doubleReader.map(x => x + "!")

    val composedReader: Reader[SpecialNumber, (Int, String)] =
      for {
        x <- doubleReader
        y <- bang
      } yield (x, y)

    val theFn: SpecialNumber => (Int, String) = composedReader.run

    val (aInt: Int, aString: String) = theFn(10)
  }

  def usage() = {

    import cats.data.Reader

    final case class Context(account: String, guid: String)

    type Channels = List[String]
    type ContextReader[A] = Reader[Context, A]


    def getChannels: ContextReader[Channels] = for {
      lineup <- getLineup
      channels <- getChannel(lineup)
    } yield channels

    def getLineup: ContextReader[Int] = Reader {
      (x: Context) => 42
    }

    def getChannel(lineup: Int): ContextReader[Channels] = Reader {
      (x: Context) => List(s"BarneyChannel $lineup")
    }

    val program: Context => Channels = getChannels.run

    val result: Channels = program(Context("account", "guid"))
  }

  def exp() = {
    final case class Config(configStuff: String)

    def configFiddler1(c: Config) = s"${c.configStuff} 1"
    def configFiddler2(c: Config) = s"${c.configStuff} 2"

    val config = Config("stuff")
    val a = configFiddler1(config)
    val b = configFiddler2(config)

    val result = s"$a $b"
  }
}
