---
title: Reader Monad in Cats
tags: scala cats functionalProgramming monad reader
---

These are my notes from [Advanced Scala with Cats](http://underscore.io/books/advanced-scala/)

## Reader
The reader monad allows you to build complex computations compositionally. It is commonly used to inject configuration into set of functions that are composed together.  In other words so you don't have to keep passing the configuration to each one of the functions.
  

```tut:silent
final case class Config(configStuff: String)

def configFiddler1(c: Config) = s"${c.configStuff} 1"
def configFiddler2(c: Config) = s"${c.configStuff} 2"

val config = Config("stuff")
val a = configFiddler1(config)
val b = configFiddler2(config)

val result = s"$a $b"
```

Reader[A,B] has two values. A is the parameter type and B is the return type.

## Creating a Reader Monad
A reader can be created from a function using the ```.apply```

```tut
import cats.data.Reader
val doubleReader: Reader[Int, Int] = Reader((x: Int) => x * 2)
```

## Extracting from the Reader Monad
You can extract the function from the reader using run.

```tut
import cats.data.Reader
val doubleReader: Reader[Int, Int] = Reader((x: Int) => x * 2)

val double: Int => Int = doubleReader.run
```

## Composing the Reader Monad

### map
```tut:silent
import cats.data.Reader
val doubleReader: Reader[Int, Int] = Reader((x: Int) => x * 2)
val bang: Reader[Int, String] = doubleReader.map(x => x + "!")
val theFn: Int => String = bang.run
val bangString: String = theFn(10)
```

How is the reader monad different than just function composition?  
```tut:silent
val doubleReader: Int => Int = (x: Int) => x * 2
val bang: Int => String = (x: Int) => x + "!"
val theFn: Int => String = bang compose doubleReader
val bangString: String = theFn(10)
```

So far map is not that interesting.  It just passes the result onto the function passed into map.

### flatMap
Flatmap can combine multiple readers that depend on the same input (the A in Reader[A,B]). In other words the functions are now provided a context.

```tut:silent
import cats.data.Reader

final case class Context(account: String, guid: String)

type Channels = List[String]
type ContextReader[A] = Reader[Context, A]

def getLineup: ContextReader[Int] = Reader {
  (x: Context) => 42
}

def getChannel(lineup: Int): ContextReader[Channels] = Reader {
  (x: Context) => List(s"BarneyChannel $lineup")
}

def getChannels: ContextReader[Channels] = for {
  lineup <- getLineup
  channels <- getChannel(lineup)
} yield channels

val program: Context => Channels = getChannels.run

val result: Channels = program(Context("account", "guid"))
```

```getChannels: ContextReader[Channels]``` is where you can see the context.  The context is literally ```Context```.  Notice that each of the functions ```getLineup``` and ```getChannels``` returns a Reader that is created with a function ```(Context) => ???```.  Remember, that flatMap can combine multiple readers that depend on the same input.  Notice that ```val program``` is of type ```Context => Channels```.  So getChannels is composing getLineup and getChannel (both take ```Context``` as a parameter) into a function (```Context => Channels```).  So the context for the two methods ```getLineup``` and ```getChannel``` is ```Context```.

So why is this good?  It assures that when those two function execute they have the same context.  The code below allows you to change the config/context between the two calls. The reader monad assures you are using the same context.

```tut:silent
final case class Config(configStuff: String)

def configFiddler1(c: Config) = s"${c.configStuff} 1"
def configFiddler2(c: Config) = s"${c.configStuff} 2"

val a = configFiddler1(Config("stuff"))
val b = configFiddler2(Config("otherstuff"))

val result = s"$a $b"
```

It will depend on your situation if you use the reader monad or not.  It is possible to just use implicit parameters to get the same effect.





## Other good reads

[reading your future](http://mergeconflict.com/reading-your-future/)
