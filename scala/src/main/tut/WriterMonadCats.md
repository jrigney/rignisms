---
title: Writer Monad in Cats
tags: scala cats functionalProgramming monad writer
---
These are my notes from [Advanced Scala with Cats](http://underscore.io/books/advanced-scala/)

## Writer Monad
The writer monad is a monad that lets you carry a log with a computation.  It can be used to record messages about decisions made in the code, error, or anything else you might want to associate with a computation.  It might work particularly in maintaining logs in a multithreaded environment.

Writer[W,A] has two values.  W is the type of the log and A is the type of the result or computation.  W should be a type that has an efficient append operation since it will be appended with new logs often.

### Creating a Writer Monad
There are several ways to create a Writer Monad.

Using the pure method since Writer is a monad.  The log, W in Writer[W,A], needs to be a Monoid since the log needs to be appended to.

```tut
import cats.data.Writer
import cats.syntax.applicative._
import cats.instances.vector._
type Logged[A] = Writer[Vector[String], A]
val a = 123.pure[Logged]
```

If you have the log and the computation you could use the apply method

```tut
import cats.data.Writer
import cats.instances.vector._
val a = Writer(Vector("msg1", "msg2", "msg3"), 123)
```

or the syntax.
```tut
import cats.data.Writer
import cats.syntax.writer._
val a = 123.writer(Vector("It all starts here.", "or maybe here"))
```

If you have only the log the you could use the tell method.  Where the computation would be Unit.
```tut
import cats.syntax.writer._
val a = Vector("msg1", "msg2", "msg3").tell
```

### Extracting the values from the Writer monad

Getting the computation

```tut
import cats.data.Writer
import cats.instances.vector._
val a = Writer(123, Vector("msg1", "msg2", "msg3"))

val comp = a.written
```

Getting the log
```tut
import cats.data.Writer
import cats.instances.vector._
val a = Writer(123, Vector("msg1", "msg2", "msg3"))

val log = a.value
```

Getting both the computation and the log at the same time
```tut
import cats.data.Writer
import cats.instances.vector._
val a = Writer(123, Vector("msg1", "msg2", "msg3"))

val (computation, log) = a.run
```

### Transforming the computation of the Writer monad <a name="transforming"></a>
Two things to remember map is log preserving and flatMap appends to the log.

```tut
import cats.syntax.writer._
import cats.data.Writer
import cats.syntax.applicative._

type Logged[A] = Writer[Vector[String], A]

val writer1 = for {
  a <- 10.pure[Logged]
  _ <- Vector("a", "b", "c").tell
  b <- 32.writer(Vector("x", "y", "z"))
} yield a + b
```

Notice the way logs are appended to ```10.pure[Logged]``` as ```_ <- Vector("a", "b", "c").tell```.  Remember, Writer.flatMap appends logs and a for comprehension is flatMap map.  You can see it better desugared.

```tut
import cats.syntax.writer._
import cats.data.Writer
import cats.syntax.applicative._

type Logged[A] = Writer[Vector[String], A]
val c = 10.pure[Logged].flatMap { ten =>
  Vector("msg1", "msg2", "msg3").tell.flatMap { _ =>
    32.writer(Vector("x", "y", "z")).map { thirtyTwo =>
      ten + thirtyTwo
    }
  }
}

```

### Transforming the logs of the Writer monad

Using mapWritten

```tut
import cats.data.Writer
val a = Writer(Vector("msg1", "msg2", "msg3"), 123)

a.mapWritten(_.map(_.toUpperCase()))
a.mapWritten(ms => "added msg" +: ms)
```

### Transforming both the computation and the log of the Writer monad

Using bimap

```tut
import cats.data.Writer
val a = Writer(Vector("msg1", "msg2", "msg3"), 123)

a.bimap(
  log => "added 2" +: log,
  comp => comp + 2
)
```

Using mapBoth
```tut
import cats.data.Writer
val a = Writer(Vector("msg1", "msg2", "msg3"), 123)

a.mapBoth((log, comp) => ("added 2" +: log, comp + 2) )
```

### Examples using the Writer monad

#### Factorial

##### Before the Writer Monad
When running two Facs in parallel the printlns will be mixed together.  
```tut:silent
import scala.concurrent._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._


object Fac extends App {

  def facBefore() = {
    def slowly[A](body: => A) =
      try body finally Thread.sleep(100)
    // slowly: [A](body: => A)A
    def factorial(n: Int): Int = {
      val ans = slowly(if(n == 0) 1 else n * factorial(n - 1))
      println(s"fact $n $ans")
      ans
    }
    Await.result(Future.sequence(Vector( 
        Future(factorial(5)), 
        Future(factorial(5))
    )), Duration.Inf)
    factorial(5)
  }

  facBefore()
}
```

##### After the Writer monad
When running the two Facs in parallel the printlns can be separated because the Writer's log attribute contains each individual string as a separate log.

```tut:silent
import cats.data.Writer
import cats.syntax.applicative._
import cats.syntax.writer._
import cats.instances.vector._

import scala.concurrent._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._


object Fac extends App {

  def facAfter() = {
    type Logged[A] = Writer[Vector[String], A]

    def slowly[A](body: => A): A = try body finally Thread.sleep(100)

    def factorial(n: Int): Logged[Int] = {
      if (n == 0) 1.pure[Logged]
      else {
        for {
          f <- slowly(factorial(n - 1))
          _ <- Vector(s"fact $n $f").tell
        } yield n * f
      }
    }

    val Vector((logA, ansA),((logB, ansB))) = Await.result(Future.sequence(Vector(
      Future(factorial(5).run),
      Future(factorial(5).run)
    )), Duration.Inf)

    println(s"A: ${logA} $ansA")
    println(s"B: ${logB} $ansB")
  }

  facAfter()
}
```

##### How to think about going from facBefore to facAfter
We want to keep ```def slowly``` to maintain the potential interleaving just like in facBefore.

```tut:silent
  def facAfter() = {
    def slowly[A](body: => A) = try body finally Thread.sleep(100)

  }
```

Knowing we want match the output from facBefore but have the logs not interleaved we are going to want both the computation and the log from the Writer monad.  So we need ```factorial(5).run``` and we will need ```def factorial(i: Int): Writer[Vector[String],String]``` as well as the import for the Writer monad ```import cats.data.Writer```.
```tut:silent
  import cats.data.Writer
  def facAfter() = {
    //want to keep slowly to maintain the potential interleaving just like in facBefore
    def slowly[A](body: => A) = try body finally Thread.sleep(100)

    def factorial(i: Int): Writer[Vector[String],Int] = ???
    Await.result(Future.sequence(Vector(
      Future(factorial(5).run),
      Future(factorial(5).run)
    )), Duration.Inf)

  }
```

Now we just need to fill in the rest of the factorial and the types can help.

The exit case for the factorial recursion needs to be a Writer because ```def factorial(i: Int): Writer[Vector[String], String] ```.  The way to get a Writer from another type is to use pure.  Using prue requires you to ```import cats.syntax.applicative._```.  Pure also takes a type constructor as a type parameter so now would be a good time to create a type ```type Logged[A] = Writer[Vector[String],A]``` and substitute it were necessary.

```tut:silent
  def facAfter() = {
    import cats.data.Writer
    import cats.syntax.applicative._
    def slowly[A](body: => A) = try body finally Thread.sleep(100)

    type Logged[A] = Writer[Vector[String],A]
    def factorial(i: Int): Logged[Int] = {
      if(i == 0) 1.pure[Logged]
      ???
    }
    
    Await.result(Future.sequence(Vector(
      //we know we want both the computation and the log from the Writer monad so it matches the facBefore
      Future(factorial(5).run),
      Future(factorial(5).run)
    )), Duration.Inf)

  }
```

Remember using the for comprehension in [Transforming the computation of the Writer monad](#transforming)?  You will need to do that for the progress case of the factorial recursion.  Don't forget to ```import cats.syntax.writer._``` for ```.tell```.  You will also need to ```import cats.instances.vector._``` for the Writer.flatMap in the for-comprehension.  Remember, the Writer.flatMap appends logs and the Semigroup typeclass is what facilitates the append.

```tut:silent
  def facAfter() = {
    import cats.data.Writer
    import cats.syntax.applicative._
    import cats.syntax.writer._
    import cats.instances.vector._
    def slowly[A](body: => A) = try body finally Thread.sleep(100)

    type Logged[A] = Writer[Vector[String], A]
    def factorial(i: Int): Logged[Int] = {
      if(i == 0) 1.pure[Logged]
      else{
        for {
            f <- slowly(factorial(i - 1))
            _ <- Vector(s"fact $i ${f*i}").tell
        }yield i * f
      }
    }
    
    Await.result(Future.sequence(Vector(
      //we know we want both the computation and the log from the Writer monad so it matches the facBefore
      Future(factorial(5).run),
      Future(factorial(5).run)
      
    )), Duration.Inf)

  }
```
Finally you can output the results

```tut:silent
  def facAfter() = {
    import cats.data.Writer
    import cats.syntax.applicative._
    import cats.syntax.writer._
    import cats.instances.vector._
    def slowly[A](body: => A) = try body finally Thread.sleep(100)

    type Logged[A] = Writer[Vector[String], A]
    def factorial(i: Int): Logged[Int] = {
      if(i == 0) 1.pure[Logged]
      else{
        for {
            f <- slowly(factorial(i - 1))
            _ <- Vector(s"fact $i ${f*i}").tell
        }yield i * f
      }
    }
    
    val Vector((logA, ansA), (logB, ansB)) = Await.result(Future.sequence(Vector(
      //we know we want both the computation and the log from the Writer monad so it matches the facBefore
      Future(factorial(5).run),
      Future(factorial(5).run)
      
    )), Duration.Inf)
    
    println(s"A: $ansA\n${logA.mkString("\n")}")
    println(s"B: $ansB\n${logB.mkString("\n")}")
  }
```
