---
title: Http4s Client GET Example
tags: scala http4s client
---

A simple client to illustrate the http4s client for GET.

```tut:silent
object ClientExample extends App {

  import org.http4s.client.Client
  import org.http4s.client.blaze._
  import org.http4s.Uri
  import scalaz.concurrent.Task

  val httpClient: Client = PooledHttp1Client()

  def keyValue(value: String): Task[String] = {
    val target = Uri.uri("http://echo.jsontest.com/key") / value
    httpClient.expect[String](target)
  }

  val keys = Vector("apple", "butter", "charles", "duff")
  val values: Task[List[String]] = Task.gatherUnordered(keys.map(keyValue))

  val result = values.onFinish(_ => httpClient.shutdown).unsafePerformSync
  println(result)

}
```

Note that ```httpClient.expect[String](target)``` returns a Task.  Meaning the http call is only being described not executed.  Remember to execute a Task you call unsafePerformSync on it.

```Task.gatherUnordered(keys.map(keyValue))``` will run parallel calls to the end point http://echo.jsontest.com/key.  You can think of Task.gatherUnordered as Future.traverse.

```
  val runThis: Task[String] = for {
    values <- values
    _ <- httpClient.shutdown
  } yield values.mkString(",")
```
We are taking advantage of Task being a Monad and using flatMap.  Since flatMap preserves sequential execution ```httpClient.shutdow``` will execute after the values task completes.  

There is a problem however. If ```values <- values``` fails then the for comprehension will short circuit and ```_ <- httpClient.shutdown``` will not execute.

instead something like
```
  val result = values.onFinish(_ => httpClient.shutdown).unsafePerformSync
```
might be better.  ```onFinish``` will execute the shutdown when the Task has completed.
