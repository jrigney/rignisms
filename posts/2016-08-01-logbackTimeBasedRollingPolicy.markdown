---
title: Adding SizeBasedTriggeringPolicy to TimeBasedRollingPolicy
---

I started out with the following but the logs were too big for a single day so I wanted to roll by size.

~~~{.xml}
    <appender name="FILE" class="ch.qos.logback.core.rolling.RollingFileAppender">
        <file>${LOG_ROOT}/${app-name}.log</file>
        <encoder>
            <pattern>${PATTERN}</pattern>
        </encoder>
        <rollingPolicy class="ch.qos.logback.core.rolling.TimeBasedRollingPolicy">
            <fileNamePattern>${LOG_ROOT}/${app-name}.log.%d.gz</fileNamePattern>
						<maxHistory>5</maxHistory>
        </rollingPolicy>
    </appender>
~~~

The first thing I tried was what I found just googling around.

~~~{.xml}
        <triggeringPolicy class="ch.qos.logback.core.rolling.SizeBasedTriggeringPolicy">
            <maxFileSize>100MB</maxFileSize>
        </triggeringPolicy>
~~~

Great.  I'll just add it to my current appender like so.

~~~{.xml}
    <appender name="FILE" class="ch.qos.logback.core.rolling.RollingFileAppender">
        <file>${LOG_ROOT}/${app-name}.log</file>
        <encoder>
            <pattern>${PATTERN}</pattern>
        </encoder>
        <rollingPolicy class="ch.qos.logback.core.rolling.TimeBasedRollingPolicy">
            <fileNamePattern>${LOG_ROOT}/${app-name}.log.%d.gz</fileNamePattern>
            <maxHistory>5</maxHistory>
        </rollingPolicy>
        <triggeringPolicy class="ch.qos.logback.core.rolling.SizeBasedTriggeringPolicy">
            <maxFileSize>100MB</maxFileSize>
        </triggeringPolicy>
    </appender>
~~~

But that did not work.  For that matter the logs stopped rolling and just kept growing.

It happens that TimeBasedRollingPolicy is both a rolling policy *and* a triggering policy.  So putting a separate triggering policy seems to have screwed the appender.

Instead I needed to find another rolling policy.  So I ended up with the following.

~~~{.xml}
    <appender name="FILE" class="ch.qos.logback.core.rolling.RollingFileAppender">
        <file>${LOG_ROOT}/${app-name}.log</file>
        <encoder>
            <pattern>${PATTERN}</pattern>
        </encoder>
        <rollingPolicy class="ch.qos.logback.core.rolling.FixedWindowRollingPolicy">
            <minIndex>1</minIndex>
            <maxIndex>5</maxIndex>
            <fileNamePattern>${LOG_ROOT}/${app-name}.log.%i.gz</fileNamePattern>
        </rollingPolicy>
        <triggeringPolicy class="ch.qos.logback.core.rolling.SizeBasedTriggeringPolicy">
            <maxFileSize>100MB</maxFileSize>
        </triggeringPolicy>
    </appender>
~~~

If you were like me and kept the fileNamePattern you will probably see an error and the log won't be appended to.  You need to change the %d which is date to %i which is integer.

An important side note.  My application failed when I started it with the new logback.xml.  At the time I had multiple logs that were greater than 10G.  It appears when the app was starting it was trying to roll the logs and could not start properly.  Deleting the logs fixed everything.
