---
title: gitignore
tags: git sbt nvim intellij
---

I wanted to create a template for gitignore since I am always creating projects and hand building them.  My requirements are not complicated. Just cover nvim, sbt, and intellij.   This isn't something unique so I googled and found [gitignore](https://www.gitignore.io/).  Just tell gitignore what you use and it will return a gitignore file.  I didn't really need everything gitignore returned for intellij so I just created my own simple version.

[dotGitignore](https://gitlab.com/snippets/22170)

