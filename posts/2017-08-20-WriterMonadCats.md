---
title: Writer Monad in Cats
tags: scala cats functionalProgramming monad writer
---
These are my notes from [Advanced Scala with Cats](http://underscore.io/books/advanced-scala/)

## Writer Monad
The writer monad is a monad that lets you carry a log with a computation.  It can be used to record messages about decisions made in the code, error, or anything else you might want to associate with a computation.  It might work particularly in maintaining logs in a multithreaded environment.

Writer[W,A] has two values.  W is the type of the log and A is the type of the result or computation.  W should be a type that has an efficient append operation since it will be appended with new logs often.

### Creating a Writer Monad
There are several ways to create a Writer Monad.

Using the pure method since Writer is a monad.  The log, W in Writer[W,A], needs to be a Monoid since the log needs to be appended to.

```scala
scala> import cats.data.Writer
import cats.data.Writer

scala> import cats.syntax.applicative._
import cats.syntax.applicative._

scala> import cats.instances.vector._
import cats.instances.vector._

scala> type Logged[A] = Writer[Vector[String], A]
defined type alias Logged

scala> val a = 123.pure[Logged]
a: Logged[Int] = WriterT((Vector(),123))
```

If you have the log and the computation you could use the apply method

```scala
scala> import cats.data.Writer
import cats.data.Writer

scala> import cats.instances.vector._
import cats.instances.vector._

scala> val a = Writer(Vector("msg1", "msg2", "msg3"), 123)
a: cats.data.WriterT[cats.Id,scala.collection.immutable.Vector[String],Int] = WriterT((Vector(msg1, msg2, msg3),123))
```

or the syntax.
```scala
scala> import cats.data.Writer
import cats.data.Writer

scala> import cats.syntax.writer._
import cats.syntax.writer._

scala> val a = 123.writer(Vector("It all starts here.", "or maybe here"))
a: cats.data.Writer[scala.collection.immutable.Vector[String],Int] = WriterT((Vector(It all starts here., or maybe here),123))
```

If you have only the log the you could use the tell method.  Where the computation would be Unit.
```scala
scala> import cats.syntax.writer._
import cats.syntax.writer._

scala> val a = Vector("msg1", "msg2", "msg3").tell
a: cats.data.Writer[scala.collection.immutable.Vector[String],Unit] = WriterT((Vector(msg1, msg2, msg3),()))
```

### Extracting the values from the Writer monad

Getting the computation

```scala
scala> import cats.data.Writer
import cats.data.Writer

scala> import cats.instances.vector._
import cats.instances.vector._

scala> val a = Writer(123, Vector("msg1", "msg2", "msg3"))
a: cats.data.WriterT[cats.Id,Int,scala.collection.immutable.Vector[String]] = WriterT((123,Vector(msg1, msg2, msg3)))

scala> val comp = a.written
comp: cats.Id[Int] = 123
```

Getting the log
```scala
scala> import cats.data.Writer
import cats.data.Writer

scala> import cats.instances.vector._
import cats.instances.vector._

scala> val a = Writer(123, Vector("msg1", "msg2", "msg3"))
a: cats.data.WriterT[cats.Id,Int,scala.collection.immutable.Vector[String]] = WriterT((123,Vector(msg1, msg2, msg3)))

scala> val log = a.value
log: cats.Id[scala.collection.immutable.Vector[String]] = Vector(msg1, msg2, msg3)
```

Getting both the computation and the log at the same time
```scala
scala> import cats.data.Writer
import cats.data.Writer

scala> import cats.instances.vector._
import cats.instances.vector._

scala> val a = Writer(123, Vector("msg1", "msg2", "msg3"))
a: cats.data.WriterT[cats.Id,Int,scala.collection.immutable.Vector[String]] = WriterT((123,Vector(msg1, msg2, msg3)))

scala> val (computation, log) = a.run
computation: Int = 123
log: scala.collection.immutable.Vector[String] = Vector(msg1, msg2, msg3)
```

### Transforming the computation of the Writer monad <a name="transforming"></a>
Two things to remember map is log preserving and flatMap appends to the log.

```scala
scala> import cats.syntax.writer._
import cats.syntax.writer._

scala> import cats.data.Writer
import cats.data.Writer

scala> import cats.syntax.applicative._
import cats.syntax.applicative._

scala> type Logged[A] = Writer[Vector[String], A]
defined type alias Logged

scala> val writer1 = for {
     |   a <- 10.pure[Logged]
     |   _ <- Vector("a", "b", "c").tell
     |   b <- 32.writer(Vector("x", "y", "z"))
     | } yield a + b
writer1: cats.data.WriterT[cats.Id,Vector[String],Int] = WriterT((Vector(a, b, c, x, y, z),42))
```

Notice the way logs are appended to ```10.pure[Logged]``` as ```_ <- Vector("a", "b", "c").tell```.  Remember, Writer.flatMap appends logs and a for comprehension is flatMap map.  You can see it better desugared.

```scala
scala> import cats.syntax.writer._
import cats.syntax.writer._

scala> import cats.data.Writer
import cats.data.Writer

scala> import cats.syntax.applicative._
import cats.syntax.applicative._

scala> type Logged[A] = Writer[Vector[String], A]
defined type alias Logged

scala> val c = 10.pure[Logged].flatMap { ten =>
     |   Vector("msg1", "msg2", "msg3").tell.flatMap { _ =>
     |     32.writer(Vector("x", "y", "z")).map { thirtyTwo =>
     |       ten + thirtyTwo
     |     }
     |   }
     | }
c: cats.data.WriterT[cats.Id,Vector[String],Int] = WriterT((Vector(msg1, msg2, msg3, x, y, z),42))
```

### Transforming the logs of the Writer monad

Using mapWritten

```scala
scala> import cats.data.Writer
import cats.data.Writer

scala> val a = Writer(Vector("msg1", "msg2", "msg3"), 123)
a: cats.data.WriterT[cats.Id,scala.collection.immutable.Vector[String],Int] = WriterT((Vector(msg1, msg2, msg3),123))

scala> a.mapWritten(_.map(_.toUpperCase()))
res0: cats.data.WriterT[cats.Id,scala.collection.immutable.Vector[String],Int] = WriterT((Vector(MSG1, MSG2, MSG3),123))

scala> a.mapWritten(ms => "added msg" +: ms)
res1: cats.data.WriterT[cats.Id,scala.collection.immutable.Vector[String],Int] = WriterT((Vector(added msg, msg1, msg2, msg3),123))
```

### Transforming both the computation and the log of the Writer monad

Using bimap

```scala
scala> import cats.data.Writer
import cats.data.Writer

scala> val a = Writer(Vector("msg1", "msg2", "msg3"), 123)
a: cats.data.WriterT[cats.Id,scala.collection.immutable.Vector[String],Int] = WriterT((Vector(msg1, msg2, msg3),123))

scala> a.bimap(
     |   log => "added 2" +: log,
     |   comp => comp + 2
     | )
res2: cats.data.WriterT[cats.Id,scala.collection.immutable.Vector[String],Int] = WriterT((Vector(added 2, msg1, msg2, msg3),125))
```

Using mapBoth
```scala
scala> import cats.data.Writer
import cats.data.Writer

scala> val a = Writer(Vector("msg1", "msg2", "msg3"), 123)
a: cats.data.WriterT[cats.Id,scala.collection.immutable.Vector[String],Int] = WriterT((Vector(msg1, msg2, msg3),123))

scala> a.mapBoth((log, comp) => ("added 2" +: log, comp + 2) )
res3: cats.data.WriterT[cats.Id,scala.collection.immutable.Vector[String],Int] = WriterT((Vector(added 2, msg1, msg2, msg3),125))
```

### Examples using the Writer monad

#### Factorial

##### Before the Writer Monad
When running two Facs in parallel the printlns will be mixed together.  
```scala
import scala.concurrent._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._


object Fac extends App {

  def facBefore() = {
    def slowly[A](body: => A) =
      try body finally Thread.sleep(100)
    // slowly: [A](body: => A)A
    def factorial(n: Int): Int = {
      val ans = slowly(if(n == 0) 1 else n * factorial(n - 1))
      println(s"fact $n $ans")
      ans
    }
    Await.result(Future.sequence(Vector( 
        Future(factorial(5)), 
        Future(factorial(5))
    )), Duration.Inf)
    factorial(5)
  }

  facBefore()
}
```

##### After the Writer monad
When running the two Facs in parallel the printlns can be separated because the Writer's log attribute contains each individual string as a separate log.

```scala
import cats.data.Writer
import cats.syntax.applicative._
import cats.syntax.writer._
import cats.instances.vector._

import scala.concurrent._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._


object Fac extends App {

  def facAfter() = {
    type Logged[A] = Writer[Vector[String], A]

    def slowly[A](body: => A): A = try body finally Thread.sleep(100)

    def factorial(n: Int): Logged[Int] = {
      if (n == 0) 1.pure[Logged]
      else {
        for {
          f <- slowly(factorial(n - 1))
          _ <- Vector(s"fact $n $f").tell
        } yield n * f
      }
    }

    val Vector((logA, ansA),((logB, ansB))) = Await.result(Future.sequence(Vector(
      Future(factorial(5).run),
      Future(factorial(5).run)
    )), Duration.Inf)

    println(s"A: ${logA} $ansA")
    println(s"B: ${logB} $ansB")
  }

  facAfter()
}
```

##### How to think about going from facBefore to facAfter
We want to keep ```def slowly``` to maintain the potential interleaving just like in facBefore.

```scala
  def facAfter() = {
    def slowly[A](body: => A) = try body finally Thread.sleep(100)

  }
```

Knowing we want match the output from facBefore but have the logs not interleaved we are going to want both the computation and the log from the Writer monad.  So we need ```factorial(5).run``` and we will need ```def factorial(i: Int): Writer[Vector[String],String]``` as well as the import for the Writer monad ```import cats.data.Writer```.
```scala
  import cats.data.Writer
  def facAfter() = {
    //want to keep slowly to maintain the potential interleaving just like in facBefore
    def slowly[A](body: => A) = try body finally Thread.sleep(100)

    def factorial(i: Int): Writer[Vector[String],Int] = ???
    Await.result(Future.sequence(Vector(
      Future(factorial(5).run),
      Future(factorial(5).run)
    )), Duration.Inf)

  }
```

Now we just need to fill in the rest of the factorial and the types can help.

The exit case for the factorial recursion needs to be a Writer because ```def factorial(i: Int): Writer[Vector[String], String] ```.  The way to get a Writer from another type is to use pure.  Using prue requires you to ```import cats.syntax.applicative._```.  Pure also takes a type constructor as a type parameter so now would be a good time to create a type ```type Logged[A] = Writer[Vector[String],A]``` and substitute it were necessary.

```scala
  def facAfter() = {
    import cats.data.Writer
    import cats.syntax.applicative._
    def slowly[A](body: => A) = try body finally Thread.sleep(100)

    type Logged[A] = Writer[Vector[String],A]
    def factorial(i: Int): Logged[Int] = {
      if(i == 0) 1.pure[Logged]
      ???
    }
    
    Await.result(Future.sequence(Vector(
      //we know we want both the computation and the log from the Writer monad so it matches the facBefore
      Future(factorial(5).run),
      Future(factorial(5).run)
    )), Duration.Inf)

  }
```

Remember using the for comprehension in [Transforming the computation of the Writer monad](#transforming)?  You will need to do that for the progress case of the factorial recursion.  Don't forget to ```import cats.syntax.writer._``` for ```.tell```.  You will also need to ```import cats.instances.vector._``` for the Writer.flatMap in the for-comprehension.  Remember, the Writer.flatMap appends logs and the Semigroup typeclass is what facilitates the append.

```scala
  def facAfter() = {
    import cats.data.Writer
    import cats.syntax.applicative._
    import cats.syntax.writer._
    import cats.instances.vector._
    def slowly[A](body: => A) = try body finally Thread.sleep(100)

    type Logged[A] = Writer[Vector[String], A]
    def factorial(i: Int): Logged[Int] = {
      if(i == 0) 1.pure[Logged]
      else{
        for {
            f <- slowly(factorial(i - 1))
            _ <- Vector(s"fact $i ${f*i}").tell
        }yield i * f
      }
    }
    
    Await.result(Future.sequence(Vector(
      //we know we want both the computation and the log from the Writer monad so it matches the facBefore
      Future(factorial(5).run),
      Future(factorial(5).run)
      
    )), Duration.Inf)

  }
```
Finally you can output the results

```scala
  def facAfter() = {
    import cats.data.Writer
    import cats.syntax.applicative._
    import cats.syntax.writer._
    import cats.instances.vector._
    def slowly[A](body: => A) = try body finally Thread.sleep(100)

    type Logged[A] = Writer[Vector[String], A]
    def factorial(i: Int): Logged[Int] = {
      if(i == 0) 1.pure[Logged]
      else{
        for {
            f <- slowly(factorial(i - 1))
            _ <- Vector(s"fact $i ${f*i}").tell
        }yield i * f
      }
    }
    
    val Vector((logA, ansA), (logB, ansB)) = Await.result(Future.sequence(Vector(
      //we know we want both the computation and the log from the Writer monad so it matches the facBefore
      Future(factorial(5).run),
      Future(factorial(5).run)
      
    )), Duration.Inf)
    
    println(s"A: $ansA\n${logA.mkString("\n")}")
    println(s"B: $ansB\n${logB.mkString("\n")}")
  }
```
