---
title: Echoing a POST body back in http4s
tags: scala functional http4s
---

## create a test for a POST
Testing a POST is not too unlike testing a GET.  The main difference is you need to create the body of the request.  The body of the Request is an EntityBody.  EntityBody is an alias to Process[Task, ByteVector] so you need to create a Process[Task, ByteVector].  Process is from scalaz.stream.Process.  You can read about scalaz.stream and Process at [Introduction to scalaz-stream](https://gist.github.com/djspiewak/d93a9c4983f63721c41c) if you are interested.  toSource is interesting and you can read about it in   [Introduction to scalaz-stream](https://gist.github.com/djspiewak/d93a9c4983f63721c41c) in the Running Streams subsection.  For our purposes though it converts a Process0[ByteVector] to a Process[Task, ByteVector] which is an EntityBody.  

```scala
import scalaz.stream.Process
import scalaz.concurrent.Task
import scodec.bits.ByteVector 
import org.http4s._
import org.http4s.dsl._

val bvWriter = org.http4s.util.ByteVectorWriter()
val bob = bvWriter << "hello bob"
val body: Process[Task,ByteVector] = Process(bob.toByteVector).toSource
val request = Request(Method.POST, uri("/internal/echo"), body = body)
```

## Create the POST route and reflect the body back in the response
Creating the POST route is not too difficult.  It is just a matter of using http4s.dsl and pattern matching.  Looking at [http4s examples](https://github.com/http4s/http4s/blob/master/examples/src/main/scala/com/example/http4s/ExampleService.scala) is a good way to get ideas on how to use the dsl.

Getting the body is a little more difficult because you need to find or define an implicit EntityDecoder for the ```.as[T]```.  If you look at the EntityDecoder object in the http4s code you will see it extends EntityDecoderInstances which has an ```implicit def text(implicit defaultCharset: Charset = DefaultCharset): EntityDecoder[String]```

Reflecting back the body is just a matter of flatMap on the Task and returning back as an Ok.

```scala
import org.http4s.{ HttpService }
import org.http4s.dsl._

val internalRoutes = HttpService {
  case request @ POST -> Root / "internal" / "echo" =>
    //.as takes an implicit EntityDecoder.
    // EntityDecoder is a typeclass so you need to implement an EntityDecoder[A]
    // fortunately there are Presupplied Encoders/Decoders in http4s
    request.as[String].flatMap(Ok(_))
}
```


