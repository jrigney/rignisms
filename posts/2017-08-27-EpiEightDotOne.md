---
title: EPI 8.1 - Search sorted array for first occurrence of k
tags: scala elementsProgrammingInterview search
---

[Elements Programming Interview](http://elementsofprogramminginterviews.com/) problem 8.1

## There is a scala package for searching sequence classes.
There is a ```scala.collection.Searching._``` that provides sequence classes with search functionality.

```scala
scala> import scala.collection.Searching._
import scala.collection.Searching._

scala> val sortedSeq = Vector(-14, -10, 2, 108, 108, 243, 285, 285, 285, 401)
sortedSeq: scala.collection.immutable.Vector[Int] = Vector(-14, -10, 2, 108, 108, 243, 285, 285, 285, 401)

scala> val searchFound = sortedSeq.search(108)
searchFound: collection.Searching.SearchResult = Found(4)

scala> val searchNotFound = sortedSeq.search(10000)
searchNotFound: collection.Searching.SearchResult = InsertionPoint(10)
```

```searchFound``` indicates that ```findThis``` was found and the index.  ```searchNotFound``` is a little more interesting because it is an ```InsertionPoint```.  Both ```Found``` and ```InsertionPoint``` are part of a sum type ```scala.collection.Searching.SearchResult```

## splitAt splits before the index.
```scala
scala> val sortedSeq = Vector(-14, -10, 2, 108, 108, 243, 285, 285, 285, 401)
sortedSeq: scala.collection.immutable.Vector[Int] = Vector(-14, -10, 2, 108, 108, 243, 285, 285, 285, 401)

scala> val (left, right) = sortedSeq.splitAt(4)
left: scala.collection.immutable.Vector[Int] = Vector(-14, -10, 2, 108)
right: scala.collection.immutable.Vector[Int] = Vector(108, 243, 285, 285, 285, 401)
```

Notice that the left side has element with indices 0,1,2,3.

## Thinking about the problem.

Binary Search when it finds the element provides no assurance that the value found is the first one in the sequence.  However you can take advantage of the fact that the sequence is sorted.  If you split the sequence at the index of the find the left side of the split will be less than or equal to the element you are looking for.

```scala
scala> val sortedSeq = Vector(-14, -10, 2, 108, 108, 243, 285, 285, 285, 401)
sortedSeq: scala.collection.immutable.Vector[Int] = Vector(-14, -10, 2, 108, 108, 243, 285, 285, 285, 401)

scala> val searchFound = sortedSeq.search(108)
searchFound: collection.Searching.SearchResult = Found(4)
```
Notice that the index returned is 4 but the first 108 index is 3.

```scala
scala> val sortedSeq = Vector(-14, -10, 2, 108, 108, 243, 285, 285, 285, 401)
sortedSeq: scala.collection.immutable.Vector[Int] = Vector(-14, -10, 2, 108, 108, 243, 285, 285, 285, 401)

scala> val searchFound = sortedSeq.search(108)
searchFound: collection.Searching.SearchResult = Found(4)

scala> val (left, right) = sortedSeq.splitAt(4)
left: scala.collection.immutable.Vector[Int] = Vector(-14, -10, 2, 108)
right: scala.collection.immutable.Vector[Int] = Vector(108, 243, 285, 285, 285, 401)
```
If you take advantage of the fact that the sequence is sorted then you will notice that splitting at ```Found(4)``` index of 4 you will get a sequence that contains values less than or equal to 108.  Now you have a smaller sequence to work with that you can search for 108.

The key piece to recognize is that you need to keep track of what was the last index you found the 108 at and you need to move to the less-than half of the sequence.


[EightDotOne](https://gitlab.com/jrigney/rignisms/blob/master/scala/src/main/scala/epi/EightDotOne.scala)

