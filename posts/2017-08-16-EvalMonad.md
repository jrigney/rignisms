---
title: Eval monad in Cats
tags: cats monad scala functionalProgramming eval
---

## Eval monad

These are my notes from [Advanced Scala with Cats](http://underscore.io/books/advanced-scala/)

The Eval monad is an abstraction over different models of evaluation.  

|                  | lazy                  | eager           |
|------------------|-----------------------|-----------------|
| memoized         | lazy val; Eval.later  | val; Eval.now   |
| non-memoized     | def; Eval.always      |                 |

The Eval monad is a sum type of Eval.later, Eval.now, and Eval.always.  The value is extracted using ```.value``` and the associated evaluation model is used.

~~~scala
import cats.Eval

val h = Eval.now(1 + 1)
//h: cats.Eval[Int] = Now(2)

h.value
//res4: Int = 2


val i = Eval.later{println("maybe later"); 42}
//i: cats.Eval[Int] = cats.Later@6e639504

val iPrime = i.value
//maybe later
//iPrime: Int = 42

i.value
//res5: Int = 42


val j = Eval.always{println("maybe later"); 42}
//j: cats.Eval[Int] = cats.Always@607178a8
val jPrime = j.value
//maybe later
//jPrime: Int = 42
j.value
//maybe later
//res5: Int = 42
~~~

## Eval map flatMap
Eval's map and flatMap makes it possible to chain computations together, similar to scala.concurrent.Future.

~~~scala
import cats.Eval

val x: Eval[Int] = Eval.now{println("Calculating A"); 40}.flatMap{ a =>
  Eval.always{println("Calculating B"); 2}.map{b =>
    println("Adding A & B")
    a + b
  }
}
//Calculating A
//x: cats.Eval[Int] = cats.Eval$$anon$8@2389dd6e

x.value
//Calculating B
//Adding A & B
//res3: Int = 42
~~~

Notice that ```val x: Eval[Int] = ....``` evaluates ```Eval.now{println("Calculating A"); 40}``` right away but the rest of the expression is not evaluated till the ```.value``` is called because the flatMap is building the Eval expression which is not evaluated till ```.value``` is called.


## Eval memoize
When chaining computations ```.memoize``` will memoize the computations evaluated before ```.memoize``` and continue to evaluate the computations after the ```.memoize``` for the subsequent calls to ```.value```

~~~scala
import cats.Eval

val qq: Eval[String] =
  Eval.always{println("Step 1"); "The cat"}.
  map{ x => println("Step 2"); s"$x sat on" }.
  memoize.
  map{x => println("Step 3"); s"$x the map"}

qq.value
//Step 1
//Step 2
//Step 3
//res7: String = The cat sat on the map

qq.value
//Step 3
//res8: String = The cat sat on the map
~~~

Notice the subsequent call to ```qq.value``` only calls ```Step 3``` and not the other steps.

~~~
qq.value
//Step 3
//res8: String = The cat sat on the map
~~~

