---
title: Comparing two Lists in Scala
tags: scala testing
---

While writing some tests I wanted to compare two Lists.

~~~
scala> val a1 = List(1,2,3)
a1: List[Int] = List(1, 2, 3)

scala> val a2 = List(1,2,3)
a2: List[Int] = List(1, 2, 3)

scala> a1.sameElements(a2)
res0: Boolean = true

scala> val a3 = List(3,2,1)
a3: List[Int] = List(3, 2, 1)

scala> a1.sameElements(a3)
res1: Boolean = false

scala> a1 == a2
res6: Boolean = true
~~~

sameElements will check the other iterable contains the same elements *in the same order*.

These will do what I want as long as I know the order of the results but if the order is different...

~~~
scala> val a3 = List(3,2,1)
a3: List[Int] = List(3, 2, 1)

scala> a1.sameElements(a3)
res1: Boolean = false

scala> a1 == a3
res5: Boolean = false
~~~

In testing I usually don't want to have to know the order of the results.

~~~
scala> a1.sorted == a3.sorted
res7: Boolean = true
~~~

It usually makes comparisons easier if you sort before comparing.

Apparently Arrays are handled differently.  I'll need to look at them more closely.

