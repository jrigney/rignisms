# Rignisms

A blog where I reflect on things in the hope of learning something

https://jrigney.gitlab.io/rignisms/

## build the site
```
$ stack build
$ stack exec site build
```

## look at the site
```
$ stack exec site watch
```

http://localhost:8000/
